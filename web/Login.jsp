<%-- 
    Document   : Login
    Created on : 07-sep-2019, 14:13:56
    Author     : Marta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link href="Css/CssLogin.css" rel="stylesheet" type="text/css"/>
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
         
    </head>
    
    <body>
      
                       
               <form action="ValidarLogin" method="POST">
                
                <h2>Inicio de Sesión</h2>
                     
                  <div class="form-group">
                    <label for="correo">Correo</label>
                    <input type="Usuario" class="form-control" id="correo" name="Usuario" placeholder="Ingrese su Correo" >
                  </div>
                
                 <div class="form-group">
                    <label for="clave">Clave</label>
                    <input type="Password" class="form-control" name="Password" id="clave" placeholder="Ingrese su contraseña">
                 </div>
                                 
                <button type="submit" name="Ingresar">Ingresar</button>
                
              
            </form>

      </body>
</html>
