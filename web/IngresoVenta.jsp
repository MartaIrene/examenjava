<%-- 
    Document   : IngresoVenta
    Created on : 07-sep-2019, 17:32:22
    Author     : Marta
--%>

<%@page import="Conexion.Conexion"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%
    Conexion cnx =  new Conexion();
    cnx.getConnection();
    
    
    ResultSet TipoVenta  =  cnx.cargarCombo("select VenId, VenTipo  from Venta");
    ResultSet cliente =  cnx.cargarCombo("select CliRut, CliNombre from Cliente");
    
    ResultSet tabla = cnx.cargarCombo("select * from vw_CargarTablaventa" );
    ResultSet resultado = cnx.cargarCombo("select * from vw_VerVenta" );
   
 %>
 
 <!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso de Ventas</title>
    <a href="Bootstrap/css/bootstrap.min.css.map"></a>
    <link href="Css/CssIngresoVenta.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <h1>Ingreso de Venta</h1>
                     
              
                     
                     
         <section id="container">     
          
                      
             <form action="GuardarIngresoventa" method="post">
                        
             
             <div class="form-row">
               <div class="form-group col-md-4">
                <label for="folio">Folio</label>
                <div class="col-sm-10">
                    <input type="text" name="folio" class="form-control" id="inputEmail3" placeholder="Ingrese Folio">
                </div>
            </div>      
             
             <div class="form-group col-md-4">
             <label for="TipoVenta">Tipo de Venta</label>
             <select name="TipoVenta" id="Venta" size="1">
             <option value="0">Seleccione tipo de Venta</option>

                    <%
                        while (TipoVenta.next()) {
                            out.println("<option value='" + TipoVenta.getInt(1) + "'>");
                            out.println(TipoVenta.getString(2));
                            out.println("</option>");
                        }
                    %>
                </select>
            </div> 
             
             
                <div class="form-group col-md-4">
                <label for="Fechaventa">Fecha Venta</label>
                <input type="text" name="Fechaventa" class="form-control" id="inputEmail3"  placeholder="Año-Mes-dìa" >
                 </div> 

                       
                     
                  <div class="form-group col-md-4">  
                   <label for="Nombre">Nombre</label>
                    <select name="Nombre" id="Analisis" size="1">
                    <option value="0">Seleccione Usuario</option>

                    <%
                        while (cliente.next()) {
                            out.println("<option value='" + cliente.getInt(1) + "'>");
                            out.println(cliente.getString(2));
                            out.println("</option>");
                        }
                    %>
                </select>
            </div>   
                
                   <div class="form-group col-md-6 mt-4">
                <button type="submit" name="ventas">CREAR VENTA</button>
            </div>  
                      
         </section>                         
                 
                    <table border="1" class="table table-hover" style="width: 100%">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Tipo Venta</th>
                        <th>Fecha</th>
                        <th>Nombre</th>
                    </tr>
                </thead>   
                
                
                   <%
                    while (tabla.next()) {
                        out.println("<tr>");
                        out.println("<td>" + tabla.getString(1) + "</td>");
                        out.println("<td>" + tabla.getString(2) + "</td> ");
                        out.println("<td>" + tabla.getString(3) + "</td>");
                        out.println("<td>" + tabla.getString(4) + "</td>");
                        
                     
                        out.println("</tr>");
                    }
                %>
            </table>
                
             
</form>   
            
            
           <table border="1" class="table table-hover" style="width: 100%">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Tipo Venta</th>
                        <th>Fecha</th>
                        <th>RutCliente</th>
                        <th>NombreCliente</th>
                          <th>Monto Total de la Venta</th>
                    </tr>
                </thead>   
                
                
                   <%
                    while (resultado.next()) {
                        out.println("<tr>");
                        out.println("<td>" + resultado.getString(1) + "</td>");
                        out.println("<td>" + resultado.getString(2) + "</td> ");
                        out.println("<td>" + resultado.getString(3) + "</td>");
                        out.println("<td>" + resultado.getString(4) + "</td>");
                         out.println("<td>" + resultado.getString(5) + "</td>");
                        out.println("<td>" + resultado.getString(6) + "</td>");
                        out.println("</tr>");
                    }
                %>
            </table>
                      
            
            
            
            
            
    </div>
    </body>                  
</html>
