package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import Conexion.Conexion;

public final class IngresoDetalleventa_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>IngresoDetalleventa</title>\n");
      out.write("         <link href=\"Css/CssIngresoVenta.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        \n");
      out.write("      ");

    Conexion cnx =  new Conexion();
    cnx.getConnection();
    
    
   ResultSet producto  =  cnx.cargarCombo("select ProCodigo, ProNombre  from Producto");
   ResultSet tabla = cnx.cargarCombo("SELECT * FROM vw_Detalleventa" );
  
 
      out.write("  \n");
      out.write("        \n");
      out.write("  <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>IngresoDetalleVenta</title>\n");
      out.write("    <a href=\"Bootstrap/css/bootstrap.min.css.map\"></a>\n");
      out.write("   \n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    <h1>Ingreso de Detalle de Venta</h1>\n");
      out.write("            \n");
      out.write("    \n");
      out.write("    <form action=\"IngresoDetalleventa\" method=\"post\">\n");
      out.write("                        \n");
      out.write("             \n");
      out.write("             <div class=\"form-row\">\n");
      out.write("                 \n");
      out.write("               <div class=\"form-group col-md-3\">\n");
      out.write("                <label for=\"cantidad\">Cantidad</label>\n");
      out.write("                <div class=\"col-sm-10\">\n");
      out.write("                    <input type=\"text\" name=\"Cantidad\" class=\"form-control\" id=\"inputEmail3\" placeholder=\"Ingrese cantidad\">\n");
      out.write("                </div>\n");
      out.write("            </div>      \n");
      out.write("             \n");
      out.write("             <div class=\"form-group col-md-3\">\n");
      out.write("             <label for=\"Producto\">Producto</label>\n");
      out.write("             <select name=\"Producto\" id=\"Venta\" size=\"1\">\n");
      out.write("             <option value=\"0\">Seleccione Producto</option>\n");
      out.write("\n");
      out.write("                    ");

                        while (producto.next()) {
                            out.println("<option value='" + producto.getInt(1) + "'>");
                            out.println(producto.getString(2));
                            out.println("</option>");
                        }
                    
      out.write("\n");
      out.write("                </select>\n");
      out.write("            </div> \n");
      out.write("             \n");
      out.write("             \n");
      out.write("                <div class=\"form-group col-md-3\">\n");
      out.write("                <label for=\"Descuento\">% Descuento</label>\n");
      out.write("                <input type=\"text\" name=\"Descuento\" class=\"form-control\" id=\"inputEmail3\" placeholder=\"Ingrese descuento\"  >\n");
      out.write("                 </div> \n");
      out.write("\n");
      out.write("                                        \n");
      out.write("                             \n");
      out.write("                 <div class=\"form-group col-md-3 mt-4\">\n");
      out.write("                <button type=\"submit\" name=\"mas\">+</button>\n");
      out.write("                 </div>  \n");
      out.write("                        \n");
      out.write("                 \n");
      out.write("                    <table border=\"1\" class=\"table table-hover\" style=\"width: 100%\">\n");
      out.write("                <thead>\n");
      out.write("                    <tr>\n");
      out.write("                        <th>Cantidad</th>\n");
      out.write("                        <th>Producto</th>\n");
      out.write("                        <th>PrecioUnitario</th>\n");
      out.write("                        <th>Total</th>\n");
      out.write("                        <th>Quitar</th>\n");
      out.write("                    </tr>\n");
      out.write("                </thead>   \n");
      out.write("                \n");
      out.write("                \n");
      out.write("                   ");

                    while (tabla.next()) {
                        out.println("<tr>");
                        out.println("<td>" + tabla.getString(1) + "</td>");
                        out.println("<td>" + tabla.getString(2) + "</td> ");
                        out.println("<td>" + tabla.getString(3) + "</td>");
                        out.println("<td>" + tabla.getString(4) + "</td>");                               
                        out.println("<td><input type='button' value='QUITAR'  class='btn btn-primary'>");
                                out.println("</td>");
                                              
                        out.println("</tr>");
                    }
                
      out.write("\n");
      out.write("            </table>\n");
      out.write("             </div>       \n");
      out.write("             \n");
      out.write("             <div class=\"form-row\">\n");
      out.write("                 \n");
      out.write("               <div class=\"form-group col-md-3\">\n");
      out.write("                <label for=\"ValorNeto\">Valor Neto</label>\n");
      out.write("                <div class=\"col-sm-10\">\n");
      out.write("                    <input type=\"text\" name=\"ValorNeto\" class=\"form-control\" id=\"inputEmail3\" >\n");
      out.write("                </div>\n");
      out.write("            </div>      \n");
      out.write("                        \n");
      out.write("               <div class=\"form-group col-md-3\">\n");
      out.write("                <label for=\"Iva\">IVA (19%)</label>\n");
      out.write("                <div class=\"col-sm-10\">\n");
      out.write("                    <input type=\"text\" name=\"Iva\" class=\"form-control\" id=\"inputEmail3\" >\n");
      out.write("                </div>\n");
      out.write("            </div> \n");
      out.write("                 \n");
      out.write("            \n");
      out.write("             <div class=\"form-group col-md-3\">\n");
      out.write("                <label for=\"ValorTotal\">Valor Total</label>\n");
      out.write("                <div class=\"col-sm-10\">\n");
      out.write("                    <input type=\"text\" name=\"ValorTotal\" class=\"form-control\" id=\"inputEmail3\" >\n");
      out.write("                </div>\n");
      out.write("            </div> \n");
      out.write("                 \n");
      out.write("              <div class=\"form-group col-md-3 mt-4\">\n");
      out.write("                <button type=\"submit\" name=\"Finalizar\">Finalizar</button>\n");
      out.write("                 </div> \n");
      out.write("             </div>      \n");
      out.write("        </form>     \n");
      out.write("                <div class=\"form-row\">\n");
      out.write("             <div class=\"form-group col-md-3 m-lg-auto\">\n");
      out.write("                <a href=\"IngresoDetalleventa.jsp\"><button type=\"submit\" name=\"\">SIGUIENTE</button></a>\n");
      out.write("\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
