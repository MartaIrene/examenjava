package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Conexion.Conexion;
import java.sql.ResultSet;

public final class IngresoVenta_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

    Conexion cnx =  new Conexion();
    cnx.getConnection();
    
    
    ResultSet TipoVenta  =  cnx.cargarCombo("select VenId, VenTipo  from Venta");
    ResultSet cliente =  cnx.cargarCombo("select CliRut, CliNombre from Cliente");
    
    ResultSet tabla = cnx.cargarCombo("select * from vw_CargarTablaventa" );
    ResultSet resultado = cnx.cargarCombo("select * from vw_VerVenta" );
   
 
      out.write("\n");
      out.write(" \n");
      out.write(" <!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Ingreso de Ventas</title>\n");
      out.write("    <a href=\"Bootstrap/css/bootstrap.min.css.map\"></a>\n");
      out.write("    <link href=\"Css/CssIngresoVenta.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    <h1>Ingreso de Venta</h1>\n");
      out.write("                     \n");
      out.write("              \n");
      out.write("                     \n");
      out.write("                     \n");
      out.write("         <section id=\"container\">     \n");
      out.write("          \n");
      out.write("                      \n");
      out.write("             <form action=\"GuardarIngresoventa\" method=\"post\">\n");
      out.write("                        \n");
      out.write("             \n");
      out.write("             <div class=\"form-row\">\n");
      out.write("               <div class=\"form-group col-md-4\">\n");
      out.write("                <label for=\"folio\">Folio</label>\n");
      out.write("                <div class=\"col-sm-10\">\n");
      out.write("                    <input type=\"text\" name=\"folio\" class=\"form-control\" id=\"inputEmail3\" placeholder=\"Ingrese Folio\">\n");
      out.write("                </div>\n");
      out.write("            </div>      \n");
      out.write("             \n");
      out.write("             <div class=\"form-group col-md-4\">\n");
      out.write("             <label for=\"TipoVenta\">Tipo de Venta</label>\n");
      out.write("             <select name=\"TipoVenta\" id=\"Venta\" size=\"1\">\n");
      out.write("             <option value=\"0\">Seleccione tipo de Venta</option>\n");
      out.write("\n");
      out.write("                    ");

                        while (TipoVenta.next()) {
                            out.println("<option value='" + TipoVenta.getInt(1) + "'>");
                            out.println(TipoVenta.getString(2));
                            out.println("</option>");
                        }
                    
      out.write("\n");
      out.write("                </select>\n");
      out.write("            </div> \n");
      out.write("             \n");
      out.write("             \n");
      out.write("                <div class=\"form-group col-md-4\">\n");
      out.write("                <label for=\"Fechaventa\">Fecha Venta</label>\n");
      out.write("                <input type=\"text\" name=\"Fechaventa\" class=\"form-control\" id=\"inputEmail3\"  placeholder=\"Año-Mes-dìa\" >\n");
      out.write("                 </div> \n");
      out.write("\n");
      out.write("                       \n");
      out.write("                     \n");
      out.write("                  <div class=\"form-group col-md-4\">  \n");
      out.write("                   <label for=\"Nombre\">Nombre</label>\n");
      out.write("                    <select name=\"Nombre\" id=\"Analisis\" size=\"1\">\n");
      out.write("                    <option value=\"0\">Seleccione Usuario</option>\n");
      out.write("\n");
      out.write("                    ");

                        while (cliente.next()) {
                            out.println("<option value='" + cliente.getInt(1) + "'>");
                            out.println(cliente.getString(2));
                            out.println("</option>");
                        }
                    
      out.write("\n");
      out.write("                </select>\n");
      out.write("            </div>   \n");
      out.write("                \n");
      out.write("                   <div class=\"form-group col-md-6 mt-4\">\n");
      out.write("                <button type=\"submit\" name=\"ventas\">CREAR VENTA</button>\n");
      out.write("            </div>  \n");
      out.write("                      \n");
      out.write("         </section>                         \n");
      out.write("                 \n");
      out.write("                    <table border=\"1\" class=\"table table-hover\" style=\"width: 100%\">\n");
      out.write("                <thead>\n");
      out.write("                    <tr>\n");
      out.write("                        <th>Folio</th>\n");
      out.write("                        <th>Tipo Venta</th>\n");
      out.write("                        <th>Fecha</th>\n");
      out.write("                        <th>Nombre</th>\n");
      out.write("                    </tr>\n");
      out.write("                </thead>   \n");
      out.write("                \n");
      out.write("                \n");
      out.write("                   ");

                    while (tabla.next()) {
                        out.println("<tr>");
                        out.println("<td>" + tabla.getString(1) + "</td>");
                        out.println("<td>" + tabla.getString(2) + "</td> ");
                        out.println("<td>" + tabla.getString(3) + "</td>");
                        out.println("<td>" + tabla.getString(4) + "</td>");
                        
                     
                        out.println("</tr>");
                    }
                
      out.write("\n");
      out.write("            </table>\n");
      out.write("                \n");
      out.write("             \n");
      out.write("</form>   \n");
      out.write("            \n");
      out.write("            \n");
      out.write("           <table border=\"1\" class=\"table table-hover\" style=\"width: 100%\">\n");
      out.write("                <thead>\n");
      out.write("                    <tr>\n");
      out.write("                        <th>Folio</th>\n");
      out.write("                        <th>Tipo Venta</th>\n");
      out.write("                        <th>Fecha</th>\n");
      out.write("                        <th>RutCliente</th>\n");
      out.write("                        <th>NombreCliente</th>\n");
      out.write("                          <th>Monto Total de la Venta</th>\n");
      out.write("                    </tr>\n");
      out.write("                </thead>   \n");
      out.write("                \n");
      out.write("                \n");
      out.write("                   ");

                    while (resultado.next()) {
                        out.println("<tr>");
                        out.println("<td>" + resultado.getString(1) + "</td>");
                        out.println("<td>" + resultado.getString(2) + "</td> ");
                        out.println("<td>" + resultado.getString(3) + "</td>");
                        out.println("<td>" + resultado.getString(4) + "</td>");
                         out.println("<td>" + resultado.getString(5) + "</td>");
                        out.println("<td>" + resultado.getString(6) + "</td>");
                        out.println("</tr>");
                    }
                
      out.write("\n");
      out.write("            </table>\n");
      out.write("                      \n");
      out.write("            \n");
      out.write("            \n");
      out.write("            \n");
      out.write("            \n");
      out.write("            \n");
      out.write("    </div>\n");
      out.write("    </body>                  \n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
