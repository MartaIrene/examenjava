<%-- 
    Document   : IngresoDetalleventa
    Created on : 07-sep-2019, 19:18:02
    Author     : Marta
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.Conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>IngresoDetalleventa</title>
         <link href="Css/CssIngresoVenta.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        
      <%
    Conexion cnx =  new Conexion();
    cnx.getConnection();
    
    
   ResultSet producto  =  cnx.cargarCombo("select ProCodigo, ProNombre  from Producto");
   ResultSet tabla = cnx.cargarCombo("SELECT * FROM vw_Detalleventa" );
  
 %>  
        
  <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>IngresoDetalleVenta</title>
    <a href="Bootstrap/css/bootstrap.min.css.map"></a>
   
</head>
<body>
    <h1>Ingreso de Detalle de Venta</h1>
            
    
    <form action="IngresoDetalleventa" method="post">
                        
             
             <div class="form-row">
                 
               <div class="form-group col-md-3">
                <label for="cantidad">Cantidad</label>
                <div class="col-sm-10">
                    <input type="text" name="Cantidad" class="form-control" id="inputEmail3" placeholder="Ingrese cantidad">
                </div>
            </div>      
             
             <div class="form-group col-md-3">
             <label for="Producto">Producto</label>
             <select name="Producto" id="Venta" size="1">
             <option value="0">Seleccione Producto</option>

                    <%
                        while (producto.next()) {
                            out.println("<option value='" + producto.getInt(1) + "'>");
                            out.println(producto.getString(2));
                            out.println("</option>");
                        }
                    %>
                </select>
            </div> 
             
             
                <div class="form-group col-md-3">
                <label for="Descuento">% Descuento</label>
                <input type="text" name="Descuento" class="form-control" id="inputEmail3" placeholder="Ingrese descuento"  >
                 </div> 

                                        
                             
                 <div class="form-group col-md-3 mt-4">
                <button type="submit" name="mas">+</button>
                 </div>  
                        
                 
                    <table border="1" class="table table-hover" style="width: 100%">
                <thead>
                    <tr>
                        <th>Cantidad</th>
                        <th>Producto</th>
                        <th>PrecioUnitario</th>
                        <th>Total</th>
                        <th>Quitar</th>
                    </tr>
                </thead>   
                
                
                   <%
                    while (tabla.next()) {
                        out.println("<tr>");
                        out.println("<td>" + tabla.getString(1) + "</td>");
                        out.println("<td>" + tabla.getString(2) + "</td> ");
                        out.println("<td>" + tabla.getString(3) + "</td>");
                        out.println("<td>" + tabla.getString(4) + "</td>");                               
                        out.println("<td><input type='button' value='QUITAR'  class='btn btn-primary'>");
                                out.println("</td>");
                                              
                        out.println("</tr>");
                    }
                %>
            </table>
             </div>       
             
             <div class="form-row">
                 
               <div class="form-group col-md-3">
                <label for="ValorNeto">Valor Neto</label>
                <div class="col-sm-10">
                    <input type="text" name="ValorNeto" class="form-control" id="inputEmail3" >
                </div>
            </div>      
                        
               <div class="form-group col-md-3">
                <label for="Iva">IVA (19%)</label>
                <div class="col-sm-10">
                    <input type="text" name="Iva" class="form-control" id="inputEmail3" >
                </div>
            </div> 
                 
            
             <div class="form-group col-md-3">
                <label for="ValorTotal">Valor Total</label>
                <div class="col-sm-10">
                    <input type="text" name="ValorTotal" class="form-control" id="inputEmail3" >
                </div>
            </div> 
                 
              <div class="form-group col-md-3 mt-4">
                <button type="submit" name="Finalizar">Finalizar</button>
                 </div> 
             </div>      
        </form>     
                <div class="form-row">
             <div class="form-group col-md-3 m-lg-auto">
                <a href="IngresoDetalleventa.jsp"><button type="submit" name="">SIGUIENTE</button></a>

                </div>

    </body>
</html>
